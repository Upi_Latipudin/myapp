/**
 * View Models used by Spring MVC REST controllers.
 */
package com.test.technical.web.rest.vm;
